#include "ConcaveToConvex.hpp"

using namespace std;
using namespace MainApplication;

namespace GeDiM
{
  // ***************************************************************************
  bool ConcaveToConvex::IsPolygonConvex(const IPolygon& polygon, const double& tolerance)
  {
    vector<const Vertex*> vertices = polygon.Vertices(); //vertices vector
    unsigned int numVertices = polygon.NumberOfVertices(); //vertices number

    bool isPolygonConvex = true;

    for(int i = 0; i < numVertices && isPolygonConvex; i++)
    {
        unsigned int one = i, two = i+1, three = i+2;
        if(two >= numVertices)
            two = two - numVertices;
        if(three >= numVertices)
            three = three - numVertices;

        Vector3d vectorFirstEdge = *vertices[two] - *vertices[one];
        Vector3d vectorSecondEdge = *vertices[three] - *vertices[two];
        double crossProduct = (vectorFirstEdge.x() * vectorSecondEdge.y()) - (vectorFirstEdge.y() * vectorSecondEdge.x());
        if(crossProduct<0)
            isPolygonConvex = false;
        }

    if (isPolygonConvex)
        return true;
    else
        return false;
  }
  // ***************************************************************************
  Output::ExitCodes ConcaveToConvex::ConcaveToConvexPolygon(const IPolygon& concavePolygon, list<IPolygon*>& convexPolygons, const double& tolerance)  ///tolerance!!!!!!!!
  {
    ///convexPolygons.push_back(concavePolygon.Clone()); ///lista di poligoni convessi
    ///ricordati di settare la tollerance!!!

    vector<const Vertex*> vertices = concavePolygon.Vertices(); //vertices vector
    unsigned int numVertices = concavePolygon.NumberOfVertices(); //vertices number
    vector<const ISegment*> segments = concavePolygon.Segments(); //segments vector
    unsigned int numSegments = concavePolygon.NumberOfSegments(); //segments number

    bool angleLeftBehind;

    do{
        angleLeftBehind = false;

        for(int i = 0; i < numVertices; i++)
        {
            unsigned int one = i, two = i+1, three = i+2, four = i+3;
            if(two >= numVertices)
                two = two - numVertices;
            if(three >= numVertices)
                three = three - numVertices;
            if(four >= numVertices)
                four = four - numVertices;



            Vector3d vectorFirstEdge = *vertices[two] - *vertices[one];
            Vector3d vectorSecondEdge = *vertices[three] - *vertices[two];
            double crossProduct = (vectorFirstEdge.x() * vectorSecondEdge.y()) - (vectorFirstEdge.y() * vectorSecondEdge.x()); //prodotto vettoriale, angolo nel vertice "two"

            if(crossProduct < 0) //se l'angolo � concavo
            {
                Vector3d vectorThirdEdge = *vertices[four] - *vertices[three];
                double cP = (vectorSecondEdge.x() * vectorThirdEdge.y()) - (vectorSecondEdge.y() * vectorThirdEdge.x()); //prodotto vettoriale dell'angolo successivo, "three"

                if(cP >= 0)  //l'angolo successivo � convesso e quindi si pu� creare un sotto poligono convesso
                {
                    Polygon *subPolygon = new Polygon();
                    convexPolygons.push_back(subPolygon);

                    Vertex *u = new Vertex(*vertices[two]);
                    Vertex *v = new Vertex(*vertices[three]);
                    Vertex *w = new Vertex(*vertices[four]);

                    (convexPolygons.back())->AddVertex(*u);
                    (convexPolygons.back())->AddVertex(*v);
                    (convexPolygons.back())->AddVertex(*w);

                    Segment *segment = new Segment();
                    segment->Initialize(*vertices[two], *vertices[four]);

                    (convexPolygons.back())->AddSegment(*segments[two]);
                    (convexPolygons.back())->AddSegment(*segments[three]);
                    (convexPolygons.back())->AddSegment(*segment);

                    //modifico il concavePolygon in modo da togliere la parte del sottopoligono appena creato

                    vertices.erase(vertices.begin()+three);
                    segments.erase(segments.begin()+two, segments.begin()+three);

                    Segment segment_;
                    segment_.Initialize(*vertices[two], *vertices[four]);
                    segments.insert(segments.begin()+two, &segment_);

                    numVertices--;
                    numSegments--;
                    i--;    //ripetere il ciclo sullo stesso angolo perch� non � detto che ora sia convesso




                }else{

                angleLeftBehind = true; //se l'angolo successivo � concavo non si pu� ancora creare un sottopoligono

                }


            }


        }

    }while(angleLeftBehind);

    //aggiungo il poligono rimanente alla lista di poligoni
    Polygon *remainingPolygon = new Polygon();
    convexPolygons.push_back(remainingPolygon);
    (convexPolygons.back())->Initialize(numVertices, numSegments);
    (convexPolygons.back())->AddVertices(vertices);
    (convexPolygons.back())->AddSegments(segments);

    return Output::Success;
  }
  // ***************************************************************************
  bool ConcaveToConvex::IsPolyhedronConvex(const IPolyhedron& polyhedron, const double& tolerance)
  {
    return false;
  }
  // ***************************************************************************
  Output::ExitCodes ConcaveToConvex::ConcaveToConvexPolyhedron(const IPolyhedron& concavePolyhedron, list<IPolyhedron*>& convexPolyhedra, const double& tolerance)
  {
    convexPolyhedra.push_back(concavePolyhedron.Clone());

    return Output::UnimplementedMethod;
  }
  // ***************************************************************************

}
